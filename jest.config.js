module.exports = {
    reporters: ['default',  ['jest-sonar', {
        outputDirectory: 'reports',
        outputName: 'test-reporter.xml',
    }]],
    "coverageDirectory": "./reports/coverage",
    "collectCoverageFrom": ["**/*.js", "!**/node_modules/**"],
    "coverageReporters": ["html", "text", "text-summary", "lcov"],
    "testMatch": [
        "**/?(*.)+(test).js"
    ],
    "resetMocks": true,
    "clearMocks": true,
    "testTimeout": 150000
}