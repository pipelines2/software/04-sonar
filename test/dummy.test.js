const dummyfunc = require('../controller/dummy.controller.js')

describe("Controller dummy function", () => {
    test("it should return input", () => {
        const input = "hello"
        const output = "hello"
        expect(dummyfunc(input)).toEqual(output);
    });
  });